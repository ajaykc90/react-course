import React, { Component } from 'react';
import { Navbar, NavbarBrand , Jumbotron , Nav, NavbarToggler, Collapse, NavItem, Button,Modal, ModalHeader, ModalBody, FormGroup,Form,Input,Label} from 'reactstrap';
import { NavLink } from 'react-router-dom';
class Header extends Component{
    constructor(props) {
        super(props);
        this.state={
            isNavOpen: false,
            isModalOpen: false
        };
        this.toggleNav = this.toggleNav.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    toggleNav(){
        this.setState({
            isNavOpen: !this.state.isNavOpen
        })
    }

    toggleModal(){
        this.setState({
            isModalOpen: !this.state.isModalOpen
        })

    }

    handleLogin(event){
        this.toggleModal();
        alert("Username:"+this.username.value +" password: "+this.password.value
        +" Remember :"+this.remember.checked);
        event.preventDefault();

          
    }
    render()
{
return(
    <React.Fragment>
    <Navbar dark expand ="md">
        <div className ="container">
            <NavbarToggler onClick={this.toggleNav} />
          <NavbarBrand  className ="mr-auto" href="/">
          <img src = "assets/images/logo.png" height ="30" width="41" 
            alt ="Ristorane Con fusion"/>
            </NavbarBrand>
            <collapse isOpen ={this.state.isNavOpen} navbar>
            <Nav navbar> 
            <NavItem>
                <NavLink className="nav-link" to ="/home"></NavLink>
                <span className ="fa fa-home fa-lg"></span> Home
            </NavItem>
            <NavItem>
                <NavLink className="nav-link" to ="/aboutus"></NavLink>
                <span className ="fa fa-info fa-lg"></span> About us
            </NavItem>
            <NavItem>
                <NavLink className="nav-link" to ="/menu"></NavLink>
                <span className ="fa fa-list fa-lg"></span> Menu
            </NavItem>
            <NavItem>
                <NavLink className="nav-link" to ="/contactus"></NavLink>
                <span className ="fa fa-address-card fa-lg"></span> Contact Us
            </NavItem>
            </Nav>
            <Nav className="ml-auto" navbar>
                <NavItem>
                    <Button outline onClick={this.toggleModal}>
                        <span className="fa fa-sign-in fa-lg"></span>Login
                    </Button>
                </NavItem>
            </Nav>
            </collapse>
        </div>
      </Navbar>
      <Jumbotron>
          <div className = "container">
              <div className ="row row-header">
                  <div className="col-12 col-sm-6">
                      <h1> Ristorante Con fusion</h1>
                      <p>We take inspiration from the World's best cuisine, and create a unique fashion experience. Our lipsmacking creation will tickle your culinary senses! </p>
                  </div>
              </div>
          </div>
      </Jumbotron>
      <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
          <ModalHeader toggle={this.toggleModal}>Login</ModalHeader>
          <ModalBody>
              <Form onSubmit={this.handleLogin}>
                  <FormGroup>
                      <Label htmlfor="username">Username</Label>
                      <Input type="text" id ="username" name ="username" innerRef ={(input)=>this.username = input}>
                      </Input>
                  </FormGroup>
                  <FormGroup>
                      <Label htmlfor="password">Password</Label>
                      <Input type="password" id ="password" name ="password" innerRef ={(input)=>this.password = input}>
                      </Input>
                  </FormGroup>
                  <FormGroup check>
                      <Label check>
                          <Input type="checkbox" name ="remember" innerRef ={(input)=>this.remember = input}/>
                          Remember me
                      </Label>
                  </FormGroup>
                  <Button type="submit" value="submit" color="bg-primary">Login</Button>
              </Form>

          </ModalBody>
      </Modal>

    </React.Fragment>
);
}
}

export default Header;